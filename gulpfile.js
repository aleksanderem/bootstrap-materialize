var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var notify = require("gulp-notify");
const imagemin = require('gulp-imagemin');
var concat = require('gulp-concat');

var paths = ['sass/modules','sass/partials'];


gulp.task('style_css', function(){
	gulp.src('sass/style.scss')
     .pipe(plumber({errorHandler: notify.onError("Błąd - sprawdź konsolę: <%= error %> <%= error.message %>")}))
	   .pipe(sass({
	   	includePaths:paths
	   }))
     .pipe(gulp.dest('css/'))
     .pipe(browserSync.stream());
	
});

gulp.task('rwd_css',function(){
	gulp.src('sass/rwd.scss')
     .pipe(plumber({errorHandler: notify.onError("Błąd - sprawdź konsolę: <%= error %> <%= error.message %>")}))
	   .pipe(sass({
	   	includePaths:paths
	   }))
      .pipe(gulp.dest('css/'))
      .pipe(browserSync.stream());
});
gulp.task('scripts', function() {
          return gulp.src(
                [
                  "./js/develop/initial.js",
                  "./js/develop/jquery.easing.1.3.js",
                  "./js/develop/animation.js",
                  "./js/develop/velocity.min.js",
                  "./js/develop/hammer.min.js",
                  "./js/develop/jquery.hammer.js",
                  "./js/develop/global.js",
                  "./js/develop/collapsible.js",
                  "./js/develop/dropdown.js",
                  "./js/develop/leanModal.js",
                  "./js/develop/materialbox.js",
                  "./js/develop/parallax.js",
                  "./js/develop/tabs.js",
                  "./js/develop/tooltip.js",
                  "./js/develop/waves.js",
                  "./js/develop/toasts.js",
                  "./js/develop/sideNav.js",
                  "./js/develop/scrollspy.js",
                  "./js/develop/forms.js",
                  "./js/develop/slider.js",
                  "./js/develop/cards.js",
                  "./js/develop/chips.js",
                  "./js/develop/pushpin.js",
                  "./js/develop/buttons.js",
                  "./js/develop/transitions.js",
                  "./js/develop/scrollFire.js",
                  "./js/develop/date_picker/picker.js",
                  "./js/develop/date_picker/picker.date.js",
                  "./js/develop/character_counter.js",
                  "./js/develop/carousel.js",
                ]
            )
            .pipe(concat({path:'materialize.js',newLine:';'}))
            .pipe(gulp.dest('js/'))
            .on('error', function(){notify.onError("Błąd - sprawdź konsolę: <%= error %> <%= error.message %>")});
        });

gulp.task('serve', ['style_css','rwd_css','scripts'], function() {

    browserSync.init({
        server: "./"
    });

        gulp.watch('sass/**/*.scss', ['style_css']);
	    gulp.watch('sass/**/*.scss', ['rwd_css']);
        gulp.watch("*.html").on('change', browserSync.reload);
        gulp.watch("js/*.js").on('change', browserSync.reload);

        

        gulp.watch("img/uncompressed/*",function(){
        gulp.src('img/uncompressed/*')
        .pipe(imagemin())
        .pipe(gulp.dest('img'))
    });
    
});


gulp.task('watch',function(){
	gulp.watch('sass/**/*.scss', ['style_css']);
	gulp.watch('sass/**/*.scss', ['rwd_css']);
});

gulp.task('default',['serve']);